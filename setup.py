import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

with open('requirements.txt', mode='r', encoding='utf-8') as f:
    required = f.read().splitlines()

setup(
    name = "soaptools",
    version = "v2.0.0",
    author = "Adam Connor/ASMP-INT",
    author_email = "adam.connor@austin.utexas.edu",
    description = ("Useful soap-related utilities"),
    license = "Copyright 2015 The University of Texas",
    url = "https://code.its.utexas.edu/gitblit/summary/?r=ASMP_INT/soaptools.git",
    packages=find_packages(),
    install_requires=required,
    long_description = read("README.md"),
    classifiers=["Development Status :: 4 - Beta"],
)

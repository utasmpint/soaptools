import os

import pytest


def pytest_ignore_collect(path, config):
    tests_dir = os.path.abspath('tests/')
    if not str(path).startswith(tests_dir):
        # print('>>>>> Rejecting', path)
        return True
    return False

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

We try to adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.1] - 2022-12-08
### Improvements
 - More up-to-date README.md

## [v2.0.0] - 2022-12-07
### Improvements
 - Removed the send\_parallel\_requests of SoapRequest. No remaining grequests dependencies.

## [v1.1_py3.8] - 2022-10-25
### Improvements
 - Made the "optional" branch the master branch, and made changees to support it for Python 3.8.

## [optional\_grequests\_v1.0.2] - 2020-04-13
### Improvements
 - Made "optional" grequests really optional, i.e., took them out of setup.py dependencies.

## [optional\_grequests\_v1.0.1] - 2020-04-13
### Improvements
 - Updated the version tag in setup.py.

## [optional\_grequests\_v1.0.0] - 2020-04-10
 - Optional branch that enabled grequests.

## [v1.0.0] - 2018-03-23
- First stable release.


## [v0.1.0] - 2015-11-05
- Initial release/Proof-of-concept (POC)

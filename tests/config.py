import functools
import os
import sys

from soaptools.utils.dict_proxy import DictProxy


first_print = None

def find_config():
    global first_print
    first_print = functools.partial(print, '\n\n')
    def nop(**kwargs):
        pass

    def separate_on_console(**kwargs):
        global first_print
        # nonlocal first_print
        first_print(**kwargs) #pylint: disable=used-before-assignment
        first_print = nop

    def message(msg, **kwargs):
        separate_on_console(**kwargs)
        print(msg, **kwargs)

    try:
        import yaml
    except ImportError:
        message('PyYAML must be installed to run tests.',
                file=sys.stderr
        )
        raise

    config_path = os.environ.get('SOAP_TEST_CONFIG')
    message('>> SOAP_TEST_CONFIG: ' + str(config_path or ''))
    if config_path and not os.path.exists(config_path):
        message('Unable to use the configuration path '
                'specified in the environment variable '
                'SOAP_TEST_CONFIG'
        )
        return None # we bail if you specified a path that doesn't exist

    if config_path is None:
        config_path = os.path.join(os.getcwd(), 'test_config.yaml')
        if not os.path.exists(config_path):
            config_path = ''
            while not os.path.exists(config_path):
                if config_path != '':
                    print('> "{0}" is not a valid path.'.format(config_path), file=sys.stderr)

                separate_on_console()
                config_path = input('> Enter a test config path? (Press enter for no) ')
                if config_path == '':
                    break

    if config_path == '':
        return None
    else:
        with open(config_path, mode='r', encoding='utf-8') as f:
            cfg = yaml.safe_load(f)
            separate_on_console()
            print('> Using Configuration file at "{0}"\n'.format(config_path))
            attrs = ('WD_BASE_URL', 'WD_TENANT', 'WD_API_VERSION',
                     'username', 'password', 'worker_id', 'print_details',
                     'cs_username', 'cs_password'
            )
            proxy = DictProxy(cfg, none_attrs=attrs)
            proxy.username = '{0}@{1}'.format(proxy.WD_ISU, proxy.WD_TENANT)
            return proxy

def get_config():
    cfg = find_config()
    if cfg:
        return cfg
    else:
        return None

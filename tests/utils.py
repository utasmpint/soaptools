class ConfigurationError(Exception):
    pass


def get_wd_url(config, area):
    base_url = config.WD_BASE_URL
    if base_url is not None and base_url.endswith('/'):
        base_url = base_url[0:-1]

    if (base_url is None or
        config.WD_TENANT is None or
        config.WD_API_VERSION is None):
        raise ConfigurationError(
            'Not all WD url variables are present in the configuration.'
        )

    return '/'.join([base_url, config.WD_TENANT, area,
                     config.WD_API_VERSION]
    )

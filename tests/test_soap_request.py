"""
Integration test for soap_request.

Note that these tests look for a configuration in the current working directory
named test_config.yaml, and require that PyYAML be installed to run.
"""
import sys
import unittest
from unittest.mock import patch

from lxml import etree #pylint: disable=no-name-in-module

from soaptools.soap_request import (
    SoapRequest,
    MissingResponse,
)

from soaptools.utils.dict_proxy import DictProxy

from tests.config import get_config
from tests.utils import ConfigurationError, get_wd_url


config = get_config()
if not config:
    raise ConfigurationError('Unable to find configuration')

# Configuration = namedtuple('Config', ('username', 'password', 'worker_id', 'print_details', 'cs_username', 'cs_password'))


class TestWorkdaySoapRequest(unittest.TestCase):
    namespaces = {'env': 'http://schemas.xmlsoap.org/soap/envelope/',
                  'wd': 'urn:com.workday/bsvc'}

    def setUp(self):
        pass

    @unittest.skipUnless(config and config.worker_id, 'No worker_id to request.')
    def test_wd_soap_request_using_template_body(self):
        xml = """
        <bsvc:Get_Workers_Request bsvc:version="{wd_api_version}">
         <bsvc:Request_References bsvc:Skip_Non_Existing_Instances="true">
            <!--1 or more repetitions:-->
               <bsvc:Worker_Reference>
                  <bsvc:ID bsvc:type="Employee_ID">{emp_id}</bsvc:ID>
               </bsvc:Worker_Reference>
         </bsvc:Request_References>
         <!--Optional:-->
         <bsvc:Response_Filter>
            <!-- Use Today -->
            <bsvc:As_Of_Effective_Date>2015-10-20</bsvc:As_Of_Effective_Date>
         </bsvc:Response_Filter>

         <!--Optional:-->
         <bsvc:Response_Group>
            <bsvc:Include_Reference>0</bsvc:Include_Reference>
            <bsvc:Include_Personal_Information>0</bsvc:Include_Personal_Information>
            <bsvc:Include_Employment_Information>1</bsvc:Include_Employment_Information>
            <bsvc:Include_Compensation>0</bsvc:Include_Compensation>
            <bsvc:Include_Organizations>0</bsvc:Include_Organizations>

            <bsvc:Include_Roles>0</bsvc:Include_Roles>
            <bsvc:Include_Management_Chain_Data>0</bsvc:Include_Management_Chain_Data>
            <bsvc:Include_Multiple_Managers_in_Management_Chain_Data>0</bsvc:Include_Multiple_Managers_in_Management_Chain_Data>
            <bsvc:Include_Benefit_Enrollments>0</bsvc:Include_Benefit_Enrollments>
            <bsvc:Include_Benefit_Eligibility>0</bsvc:Include_Benefit_Eligibility>
            <bsvc:Include_Related_Persons>0</bsvc:Include_Related_Persons>
            <bsvc:Include_Qualifications>0</bsvc:Include_Qualifications>
            <bsvc:Include_Employee_Review>0</bsvc:Include_Employee_Review>
            <bsvc:Include_Goals>0</bsvc:Include_Goals>
            <bsvc:Include_Development_Items>0</bsvc:Include_Development_Items>
            <bsvc:Include_Skills>0</bsvc:Include_Skills>
            <bsvc:Include_Photo>0</bsvc:Include_Photo>
            <bsvc:Include_Worker_Documents>0</bsvc:Include_Worker_Documents>
            <bsvc:Include_Transaction_Log_Data>0</bsvc:Include_Transaction_Log_Data>
            <bsvc:Include_Succession_Profile>0</bsvc:Include_Succession_Profile>
            <bsvc:Include_Talent_Assessment>0</bsvc:Include_Talent_Assessment>
            <bsvc:Include_Employee_Contract_Data>0</bsvc:Include_Employee_Contract_Data>
            <bsvc:Include_Collective_Agreement_Data>0</bsvc:Include_Collective_Agreement_Data>
            <bsvc:Include_Probation_Period_Data>0</bsvc:Include_Probation_Period_Data>
            <bsvc:Include_Feedback_Received>0</bsvc:Include_Feedback_Received>
            <bsvc:Include_User_Account>0</bsvc:Include_User_Account>
            <bsvc:Include_Career>0</bsvc:Include_Career>
            <bsvc:Include_Account_Provisioning>0</bsvc:Include_Account_Provisioning>
            <bsvc:Include_Background_Check_Data>0</bsvc:Include_Background_Check_Data>
            <bsvc:Include_Contingent_Worker_Tax_Authority_Form_Information>0</bsvc:Include_Contingent_Worker_Tax_Authority_Form_Information>
         </bsvc:Response_Group>

        </bsvc:Get_Workers_Request>
        """.format(wd_api_version='v28.0', emp_id=config.worker_id)

        ns = {'bsvc': 'urn:com.workday/bsvc'}
        url = get_wd_url(config, 'Staffing')

        sr = SoapRequest(url,
                     config.username,
                     config.password,
                     nsmap=ns,
        )

        sr.append_to_body(xml)

        kwargs = {}
        if config.print_details:
            kwargs['debug_func'] = print
        response = sr.send_request(**kwargs)
        self.assertEqual(200, response.status_code)

    def test_wd_soap_request_using_lxml_for_body(self):
        wd_ns = 'urn:com.workday/bsvc'
        ns = {'wd': wd_ns}
        url = get_wd_url(config, 'Human_Resources')

        sr = SoapRequest(url,
                     config.username,
                     config.password,
                     nsmap=ns,
        )

        sr.build_request()
        soap_body = sr.soap_body
        #pylint: disable=no-member
        wd_request = etree.Element(etree.QName(wd_ns, 'Get_Workers_Request'))
        wd_request.set(etree.QName(wd_ns, 'version'), 'v25.0')
        wd_resp_filter = etree.Element(etree.QName(wd_ns, 'Response_Filter'))
        wd_count = etree.Element(etree.QName(wd_ns, 'Count'))

        soap_body.append(wd_request)
        wd_request.append(wd_resp_filter)
        wd_resp_filter.append(wd_count)

        kwargs = {}
        if config.print_details:
            kwargs['debug_func'] = print

        response = sr.send_request(**kwargs)
        self.assertEqual(200, response.status_code)


class TestCornerstoneSoapRequest(unittest.TestCase):
    @unittest.skipUnless(config.cs_url, 'Test skipped; no cs_url')
    @unittest.skipUnless(config.cs_username, 'Test skipped; no cs_username')
    @unittest.skipUnless(config.cs_password, 'Test skipped; no cs_password')
    def test_soap_request_with_cornerstone(self):
        xml = """
        <m:EchoOus>
           <m0:Ous>
              <m0:Ou Active="true" AllowReconcile="true" Id="MAI" Name="Main Building" Owner="Elliott, Kathryn" Parent="UTAMC" Type="Location">
                 <m0:Locality>
                    <m0:Address>
                       <m0:State>Texas</m0:State>
                    </m0:Address>
                 </m0:Locality>
              </m0:Ou>
           </m0:Ous>
           </m:EchoOus>
        """

        ns = {'m': 'urn:Cornerstone:ClientDataService',
              'm0': 'urn:Cornerstone:ClientData'}
        url = config.cs_url
        soap_action = 'EchoOus'

        sr = SoapRequest(url,
                     config.cs_username,
                     config.cs_password,
                     nsmap=ns,
                     soap_action=soap_action
        )

        sr.append_to_body(xml)

        kwargs = {}
        if config.print_details:
            kwargs['debug_func'] = print
        response = sr.send_request(**kwargs)
        self.assertEqual(200, response.status_code)

    @unittest.skipUnless(config.cs_url, 'Test skipped; no cs_url')
    @unittest.skipUnless(config.cs_username, 'Test skipped; no cs_username')
    @unittest.skipUnless(config.cs_password, 'Test skipped; no cs_password')
    def test_cornerstone_soap_request_using_lxml_for_body(self):
        cs_ns = 'urn:Cornerstone:ClientDataService'
        data_ns = 'urn:Cornerstone:ClientData'
        ns = {'m': cs_ns, 'm0': data_ns}
        url = config.cs_url
        soap_action = 'EchoOus'

        sr = SoapRequest(url,
                     config.cs_username,
                     config.cs_password,
                     nsmap=ns,
                     soap_action=soap_action
        )

        sr.build_request()
        soap_body = sr.soap_body
        #pylint: disable=no-member
        cs_request = etree.Element(etree.QName(cs_ns, 'EchoOus'))

        data_request = etree.SubElement(cs_request, etree.QName(data_ns, 'Ous'))
        ou_request = etree.SubElement(data_request, etree.QName(data_ns, 'Ou'))
        ou_request.set(etree.QName('Active'), 'true')
        ou_request.set(etree.QName('AllowReconcile'), 'true')
        ou_request.set(etree.QName('Id'), 'MAI')
        ou_request.set(etree.QName('Name'), 'Main Building')
        ou_request.set(etree.QName('Owner'), 'Elliott, Kathryn')
        ou_request.set(etree.QName('Parent'), 'UTAMC')
        ou_request.set(etree.QName('Type'), 'Location')

        ou_locality = etree.SubElement(ou_request, etree.QName(data_ns, 'Locality'))
        ou_address = etree.SubElement(ou_locality, etree.QName(data_ns, 'Address'))
        state_element = etree.SubElement(ou_address, etree.QName(data_ns, 'State'))
        state_element.text = 'Texas'

        soap_body.append(cs_request)
        cs_request.append(data_request)
        data_request.append(ou_request)
        ou_request.append(ou_locality)

        kwargs = {}
        if config.print_details:
            kwargs['debug_func'] = print

        response = sr.send_request(**kwargs)
        self.assertEqual(200, response.status_code)


    @classmethod
    def suite(cls):
        global config #pylint: disable=global-statement
        if config:
            return unittest.TestLoader().loadTestsFromTestCase(cls)
        else:
            print('cornerstone soap request tests skipped.')
            return unittest.TestSuite()

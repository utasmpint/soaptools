from base64 import b64encode
from collections import OrderedDict
from datetime import datetime
import hashlib
import os
try:
    from urllib.parse import unquote_plus #python3
except ImportError:
    from urllib import unquote_plus #python2

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

from uuid import uuid1

from lxml import etree #pylint: disable=no-name-in-module

import requests

class SoapRequestError(Exception):
    pass

class MissingResponse(object):
    def __init__(self, request, exception):
        self.request = request
        self.exception = exception


class SoapRequestParams(object):
    def __init__(self, url, debug_func, data, headers, verify):
        self.url = url
        self.debug_func = debug_func
        self.post_args = {'data': data, 'headers': headers, 'verify': verify}


class SoapRequest(object):
    SOAP = 'http://schemas.xmlsoap.org/soap/envelope/'
    WSSE = ('http://docs.oasis-open.org/wss/2004/01/'
            'oasis-200401-wss-wssecurity-secext-1.0.xsd'
    )
    WSU = ('http://docs.oasis-open.org/wss/2004/01/'
           'oasis-200401-wss-wssecurity-utility-1.0.xsd'
    )
    ENCODING_TYPE = ('http://docs.oasis-open.org/wss/2004/01/'
                     'oasis-200401-wss-soap-message-security-1.0'
                     '#Base64Binary'
    )

    PASSWORD_TYPE_DIGEST = ('http://docs.oasis-open.org/wss/2004/01'
                            '/oasis-200401-wss-username-token-profile-1.0'
                            '#PasswordDigest'
    )

    PASSWORD_TYPE_TEXT = ('http://docs.oasis-open.org/wss/2004/01'
                          '/oasis-200401-wss-username-token-profile-1.0'
                          '#PasswordText'
    )

    SOAP_NS = {'soapenv':SOAP}
    WSSE_NS = {'wsu':WSU, 'wsse':WSSE}

    def __init__(self, url, username, password, soap_action='""',
                 nsmap=None, password_digest=None, connection_value=None):
        self.url = url
        self.username = username
        self.nonce = uuid1().bytes
        self.encoded_nonce = b64encode(self.nonce)
        self.creation_time = self.iso_time()
        self.token_id = 'usernametoken-{0}'.format(uuid1().hex)
        self.password_digest = password_digest
        if password_digest: # doesn't look like WD or CS support this.
            self.password_type = self.PASSWORD_TYPE_DIGEST
            self.password_value = self.hash_password(password)
        else:
            self.password_type = self.PASSWORD_TYPE_TEXT
            self.password_value = password
        self.soap_action = soap_action
        self.soap_tree = None
        self.nsmap = nsmap
        if connection_value:
            self.connection_value = connection_value
        else:
            self.connection_value = 'close'

        pr = urlparse(url)
        self.host = pr.hostname

    def append_to_body(self, xml_string):
        # we have to supply namespaces just to get it parse
        all_ns = self.build_namespace_decls(self.nsmap)
        #pylint: disable=no-member
        ap_root = etree.XML('<content {ns}>{xml}</content>'
                            .format(ns=all_ns, xml=xml_string)
        )
        body_children = ap_root.xpath('*')
        if len(body_children):
            body = self.soap_body
            if body is None:
                self.build_request()

            for kid in body_children:
                self.soap_body.append(kid)

            if not self.validate(self.soap_body):
                raise SoapRequestError('Body does not validate.')
        else:
            raise SoapRequestError("No children were found to append.")

    @classmethod
    def build_namespace_decls(cls, nsmap):
        frags = []
        for prefix, urn in nsmap.items():
            frags.append(' xmlns:{pre}="{urn}"'
                         .format(pre=prefix, urn=urn)
            )
        all_ns = ''.join(frags)
        return all_ns

    def build_request(self, reset_body=None):

        # normally, preserve the body if it exists
        if reset_body:
            soap_body = None
        else:
            soap_body = self.soap_body

        # construct base self.elements
        soap_env = self.elem(self.SOAP, 'Envelope', nsmap=self.SOAP_NS)
        soap_header = self.elem(self.SOAP, 'Header')
        wsse_sec = self.elem(self.WSSE, 'Security', nsmap=self.WSSE_NS)
        #pylint: disable=no-member
        wsse_sec.set(etree.QName(self.SOAP, 'mustUnderstand'), "1")

        wsse_usernametoken = self.elem(self.WSSE, 'UsernameToken')
        wsse_usernametoken.set(etree.QName(self.WSU, 'Id'), self.token_id)

        wsse_username = self.elem(self.WSSE, 'Username')
        wsse_username.text = self.username
        wsse_password = self.elem(self.WSSE, 'Password')
        wsse_password.set('Type', self.password_type)
        wsse_password.text = self.password_value
        wsse_nonce = self.elem(self.WSSE, 'Nonce')
        wsse_nonce.set("EncodingType", self.ENCODING_TYPE)
        wsse_nonce.text = self.encoded_nonce
        wsu_created = self.elem(self.WSU, 'Created')
        wsu_created.text = self.creation_time

        if soap_body is None:
            soap_body = self.elem(self.SOAP, 'Body')

        soap_env.append(soap_header)
        soap_env.append(soap_body)
        soap_header.append(wsse_sec)
        wsse_sec.append(wsse_usernametoken)
        wsse_usernametoken.append(wsse_username)
        wsse_usernametoken.append(wsse_password)
        wsse_usernametoken.append(wsse_nonce)
        wsse_usernametoken.append(wsu_created)

        self.soap_tree = etree.ElementTree(soap_env)
        return self.soap_tree

    @classmethod
    def debug_response(cls, response=None, request_params=None):
        debug_func = request_params.debug_func
        if debug_func is None:
            return

        url = request_params.url
        post_args = request_params.post_args
        data = request_params.post_args.get('data')
        if data is None:
            data = b''

        headers = post_args.get('headers')
        if headers is None:
            headers = {}

        extra_headers = {}
        if debug_func and response is None:
            debug_func(
                os.linesep +
                '------------------------ Failed Request',
                '--------------------'
            )
            debug_func('{0} {1}'.format('POST', url))

            # print the headers this way to make diffing with SOAP-UI easier
            # -- preserves the order of our explicit headers
            for k, v in post_args['headers'].items():
                debug_func('{0}: {1}'.format(k, v))

            debug_func(os.linesep + data.decode('utf-8'))
        elif debug_func and response is not None:
            req = response.request
            debug_func(
                os.linesep +
                '------------------------ Request  --------------------'
            )
            debug_func('{0} {1}'.format(req.method, req.url))

            # print the headers this way to make diffing with SOAP-UI easier
            # -- preserves the order of our explicit headers
            extra_headers = {}
            for k, v in req.headers.items():
                if k not in headers:
                    extra_headers[k] = v

            for k in headers.keys():
                if k in req.headers: # sometimes they are dropped,
                                     # e.g., SOAPAction
                    debug_func('{0}: {1}'.format(k, req.headers[k]))
                else:
                    debug_func(k)

            for k, v in extra_headers.items():
                debug_func('{0}: {1}'.format(k, v))

            # when we use the requests package, we get bytes for req.body;
            # when we use grequests (parallel), we get a urlencoded string
            if isinstance(req.body, bytes):
                body = (req.body).decode('utf-8')
            else:
                body = unquote_plus(req.body, encoding='utf-8')
            debug_func(os.linesep + body)
            debug_func(
                os.linesep +
                '------------------------ Response --------------------'
            )
            debug_func('response.status_code: ' + str(response.status_code))

            if response.content:
                response_bytes = response.content
                #pylint: disable=no-member
                try:
                    root = etree.fromstring(response_bytes)
                    response_bytes = etree.tostring(root, pretty_print=True)
                except etree.LxmlError as le:
                    print(le)
                    print()
                except ValueError as ve:
                    print(ve)
                    print()

                debug_func(response_bytes.decode('utf-8'))

    @classmethod
    def elem(cls, ns, tag, nsmap=None):
        kwargs={}
        if nsmap:
            kwargs['nsmap'] = nsmap
        #pylint: disable=no-member
        return etree.Element(etree.QName(ns, tag), **kwargs)

    def get_headers(self, content):
        headers = OrderedDict()
        headers['Accept-Encoding'] = 'gzip,deflate'
        headers['Content-Type'] = 'application/xml;charset="utf-8"'
        headers['SOAPAction'] = self.soap_action
        headers['Content-Length'] = str(len(content))
        headers['Host'] = self.host
        headers['Connection'] = self.connection_value
        headers['User-Agent'] = 'UT-ASMP-INT-pysoap'

        return headers

    def get_request_params(self, request_xml=None, debug_func=None,
                           verify=True):
        #pylint: disable=no-member
        soap_body = self.soap_body
        if soap_body is None and request_xml is None:
            raise SoapRequestError("No Soap Body element.")

        kids = soap_body.xpath('*')
        if len(kids) == 0:
            raise SoapRequestError("Soap Body has no content.")

        if request_xml:
            if isinstance(request_xml, bytes):
                soap_bytes = request_xml
            else:
                soap_bytes = str(request_xml).encode('utf-8')
        else:
            kwargs = {}
            if debug_func:
                kwargs['pretty_print'] = True
            soap_bytes = etree.tostring(self.soap_tree, **kwargs)

        headers = self.get_headers(soap_bytes)
        return SoapRequestParams(url=self.url,
                                 debug_func=debug_func,
                                 data=soap_bytes,
                                 headers=headers,
                                 verify=verify
        )

    def hash_password(self, password):
        digester = hashlib.sha1()
        digester.update(self.nonce)
        digester.update(self.creation_time.encode('utf-8'))
        digester.update(password.encode('utf-8'))
        return b64encode(digester.digest())

    @classmethod
    def iso_time(cls):
        return '{0}Z'.format(datetime.utcnow().isoformat())

    @classmethod
    def pretty_print(cls, xml):
        #pylint: disable=no-member
        xml_bytes = xml.encode('utf-8')
        try:
            root = etree.fromstring(xml_bytes)
        except etree.LxmlError:
            raise SoapRequestError("Could not parse xml parameter.")
        else:
            bs = etree.tostring(root, pretty_print=True)
            s = bs.decode('utf-8')
            return s.replace('\n', os.linesep)

    def send_request(self, request_xml=None, debug_func=None, verify=True):
        req_params = self.get_request_params(request_xml, debug_func, verify)
        response = None
        try:
            response = requests.post(req_params.url, **req_params.post_args)
            return response
        finally:
            if debug_func:
                self.debug_response(response=response,
                                    request_params=req_params
                )

    @property
    def soap_body(self):
        if self.soap_tree:
            result = self.soap_tree.xpath(
                         '/soapenv:Envelope/soapenv:Body',
                         namespaces=self.SOAP_NS
            )
            if len(result) == 1:
                return result[0]
            else:
                raise SoapRequestError('Integrity error: More than one soap '
                                       'body.')
        else:
            return None

    @property
    def tree(self):
        return self.soap_tree

    def update_namespaces(self, nsmap, reset_body=None):
        """
        Replaces the original nsmap used at construction.
        NOTE: This may rebuild the request, although it will attempt
        to preserve the soap body unless reset_body is True.
        """
        self.nsmap = nsmap
        if self.soap_tree:
            self.build_request(reset_body)


    def validate(self, root=None):
        if self.soap_tree is None and root is None:
            return True

        if root is None:
            root = self.soap_tree.getroot()

        #pylint: disable=no-member
        body = etree.tostring(root)
        try:
            root = etree.fromstring(body)
        except etree.LxmlError:
            return False
        else:
            return True

from collections.abc import Mapping
from copy import copy

# this class is adapted from asmp_utils to avoid making every consumer of
# Soaptools import asmp_utils

class DictProxy(object):
    def __init__(self, dictionary, name=None, none_attrs=None):
        """
        dictionary: The dictionary to proxy. '_get_raw' should not be a key.
        name: The name to use in any attribute errors. If not provided, the
              dictionary's class name will be used.

        none_attrs: Attributes where the proxy will return none rather
              than raise an AttributeError.

        Names beginning with a single underscore (_as_dict, _get_raw,
        _get_dynamic_attributes) should be regarded as part of the API; they
        are named that way to minimize conflicts with possible dictionary keys.
        """
        self.__dict = dictionary
        if name:
            self.__name = name
        else:
            self.__name = dictionary.__class__.__name__

        if none_attrs is None:
            self.__none_attrs = ()
        else:
            self.__none_attrs = copy(none_attrs)

    def _as_dict(self):
        "Returns the underlying dict from the proxy."
        return self.__dict

    def _get_raw(self, name):
        """
        Returns the underlying dictionary value without any additional
        processing. Equivalent to self._as_dict().get(name).
        """
        return self.__dict.get(name)

    def __dir__(self):
        standard = super().__dir__()
        return standard + self._get_dynamic_attributes()

    def _get_dynamic_attributes(self):
        return list(self.__dict.keys())

    def __getattr__(self, name):
        try:
            val = self.__dict[name]
        except KeyError:
            if name in self.__none_attrs:
                return None
            else:
                raise AttributeError("'{0}' obj has no attribute '{1}'"
                                     .format(self.__name, name)
                )
        else:
            if isinstance(val, Mapping):
                return DictProxy(val, name=self.__name + '.{0}'.format(name))
            else:
                return val
